# Laporan Resmi Sistem Operasi
Sisop-praktikum-fp-2023-MH-IT23
Adimasdefatra Bimasena 5027211040

A. Authentikasi

#define MAX_QUERY_LENGTH 1000

void sendData(int sockfd, const char *data)
{
    write(sockfd, data, strlen(data) + 1);
}

void receiveData(int sockfd, char *buffer, int bufferSize)
{
    read(sockfd, buffer, bufferSize);
}

void handleAuth(int sockfd, const char *username, const char *password)
{
    // Send username and password to the server
    write(sockfd, username, strlen(username) + 1);
    write(sockfd, password, strlen(password) + 1);

    int authStatus;
read(sockfd, &authStatus, sizeof(authStatus));

if (authStatus == 0)
{
    printf("Authentication failed. Invalid username or password.\n");
    exit(1);
}

if (authStatus == -1)
{
    printf("Connection to the server has been closed.\n");
    exit(1);
}

printf("Authentication successful.\n");

}
void createUser(int sockfd, const char *username, const char *password)
{
    // Format the CREATE USER command
    char query[MAX_QUERY_LENGTH];
    snprintf(query, sizeof(query), "CREATE USER %s IDENTIFIED BY %s;", username, password);

    // Send the command to the server
    sendData(sockfd, query);

    // Read the server's response
    char response[MAX_QUERY_LENGTH];
    receiveData(sockfd, response, sizeof(response));
    printf("%s\n", response);
}

Kode ini adalah contoh implementasi fungsi-fungsi untuk mengirim dan menerima data melalui soket (socket) dalam bahasa pemrograman C. Fungsi-fungsi ini digunakan dalam konteks komunikasi dengan server untuk autentikasi pengguna dan membuat pengguna baru.

Berikut adalah penjelasan singkat tentang setiap fungsi dalam kode tersebut:

sendData(int sockfd, const char *data): Fungsi ini digunakan untuk mengirim data melalui soket. Parameternya adalah nomor soket (sockfd) yang terhubung ke server, dan data yang akan dikirimkan (data) dalam bentuk string. Fungsi ini menggunakan fungsi write() untuk menulis data ke soket.

receiveData(int sockfd, char *buffer, int bufferSize): Fungsi ini digunakan untuk menerima data dari soket. Parameternya adalah nomor soket (sockfd), buffer (buffer) untuk menyimpan data yang diterima, dan ukuran buffer (bufferSize). Fungsi ini menggunakan fungsi read() untuk membaca data dari soket ke buffer.

handleAuth(int sockfd, const char *username, const char *password): Fungsi ini mengelola proses autentikasi dengan server. Parameternya adalah nomor soket (sockfd), username (username), dan password (password) yang akan dikirimkan ke server. Fungsi ini mengirim username dan password ke server menggunakan fungsi write(), kemudian menerima status autentikasi dari server menggunakan fungsi read(). Jika status autentikasi adalah 0, maka autentikasi gagal karena username atau password tidak valid. Jika status autentikasi adalah -1, maka koneksi ke server telah ditutup. Jika status autentikasi adalah selain 0 dan -1, maka autentikasi berhasil.

createUser(int sockfd, const char *username, const char *password): Fungsi ini digunakan untuk membuat pengguna baru dengan mengirimkan perintah "CREATE USER" ke server. Parameternya adalah nomor soket (sockfd), username (username), dan password (password) untuk pengguna baru. Fungsi ini memformat perintah "CREATE USER" menggunakan fungsi snprintf(), kemudian mengirimkan perintah tersebut ke server menggunakan fungsi sendData(). Selanjutnya, fungsi ini menerima respons dari server menggunakan fungsi receiveData() dan mencetak respons tersebut.

Kode ini menggunakan fungsi write() dan read() untuk mengirim dan menerima data melalui soket. Fungsi write() digunakan untuk mengirim data dari client ke server, sementara fungsi read() digunakan untuk membaca data dari server ke client.

B. Autorisasi
Untuk fungsi autorisasi dalam program kami lewati

C. Data Definision Language

[v] CREATE DATABASE
[v] DROP DATABASE
[v] CREATE TABLE
[v] DROP TABLE
[v] INSERT INTO
[v] SELECT *
[] SELECT COLUMN
[] UPDATE DATA
[] DROP COLUMN
[] DELETE FROM TABLE (gajelas)
[] SELECT * : WHERE
[] SELECT COLUMN : WHERE
[] UPDATE : WHERE
[] DELETE : WHERE
void createDatabase(const char *dbName)
{
    char dbPath[100];
    snprintf(dbPath, sizeof(dbPath), "%s%s", DATABASES_DIR, dbName);

    struct stat st = {0};

    if (stat(DATABASES_DIR, &st) != 0)
    {
        mkdir(DATABASES_DIR, 0700);
    }

    if (stat(dbPath, &st) == 0)
    {
        printf("Database '%s' already exists.\n", dbName);
        return;
    }

    if (mkdir(dbPath, 0700) != 0)
    {
        printf("Failed to create database '%s'.\n", dbName);
        return;
    }

    if (numDatabases >= MAX_DATABASES)
    {
        printf("Maximum limit reached for databases.\n");
        return;
    }

    // Add the new database to the databases array
    Database newDatabase;
    strcpy(newDatabase.name, dbName);
    newDatabase.numTables = 0;
    databases[numDatabases] = newDatabase;
    numDatabases++;

    printf("Database '%s' created successfully.\n", dbName);
}
void createTable(const char *dbName, const char *tableName, char *columns)
{
    int dbIndex = findDatabaseIndex(dbName);
    if (dbIndex == -1)
    {
        printf("Database '%s' does not exist.\n", dbName);
        return;
    }

    Database *db = &databases[dbIndex];

    int tableIndex = findTableIndex(dbName, tableName);
    if (tableIndex != -1)
    {
        printf("Table '%s' already exists in database '%s'.\n", tableName, dbName);
        return;
    }

    if (db->numTables >= MAX_TABLES)
    {
        printf("Maximum limit reached for tables in database '%s'.\n", dbName);
        return;
    }

    Table newTable;
    strcpy(newTable.name, tableName);

    // Parsing column names
    char columnNames[MAX_COLUMNS][50];
    char *token = strtok(columns, ",");
    int numColumns = 0; // Initialize numColumns to zero

    while (token != NULL && numColumns < MAX_COLUMNS)
    {
        strcpy(columnNames[numColumns], token);
        token = strtok(NULL, ",");
        numColumns++;
    }

    // Copy column names to the new table
    for (int i = 0; i < numColumns; i++)
    {
        strcpy(newTable.columns[i], columnNames[i]);
    }

    newTable.numColumns = numColumns;

    // Add the new table to the database
    db->tables[db->numTables] = newTable;
    db->numTables++;

    // Create the table CSV file in the corresponding database directory
    char tablePath[150];
    snprintf(tablePath, sizeof(tablePath), "%s%s/%s.csv", DATABASES_DIR, dbName, tableName);

    FILE *tableFile = fopen(tablePath, "w");
    if (tableFile == NULL)
    {
        printf("Failed to create table '%s' in database '%s'.\n", tableName, dbName);
        return;
    }

    // Write the column names to the CSV file
    for (int i = 0; i < numColumns; i++)
    {
        fprintf(tableFile, "%s", newTable.columns[i]);
        if (i != numColumns - 1)
        {
            fprintf(tableFile, ",");
        }
    }

    fprintf(tableFile, "\n");
    fclose(tableFile);

    printf("Table '%s' created successfully in database '%s'.\n", tableName, dbName);
}
void dropDatabase(const char *dbName)
{
    char dbPath[100];
    snprintf(dbPath, sizeof(dbPath), "%s%s", DATABASES_DIR, dbName);

    struct stat st = {0};

    if (stat(dbPath, &st) != 0)
    {
        printf("Database '%s' does not exist.\n", dbName);
        return;
    }

    if (rmdir(dbPath) != 0)
    {
        printf("Failed to drop database '%s'.\n", dbName);
        return;
    }

    printf("Database '%s' dropped successfully.\n", dbName);
}
void dropTable(const char *dbName, const char *tableName)
{
    int dbIndex = findDatabaseIndex(dbName);
    if (dbIndex == -1)
    {
        printf("Database '%s' does not exist.\n", dbName);
        return;
    }

    Database *db = &databases[dbIndex];

    int tableIndex = findTableIndex(dbName, tableName);
    if (tableIndex == -1)
    {
        printf("Table '%s' does not exist in database '%s'.\n", tableName, dbName);
        return;
    }

    // Remove the table from the database's tables array
    for (int i = tableIndex; i < db->numTables - 1; i++)
    {
        db->tables[i] = db->tables[i + 1];
    }

    db->numTables--;

    // Delete the table CSV file in the corresponding database directory
    char tablePath[150];
    snprintf(tablePath, sizeof(tablePath), "%s%s/%s.csv", DATABASES_DIR, dbName, tableName);

    if (remove(tablePath) != 0)
    {
        printf("Failed to drop table '%s' in database '%s'.\n", tableName, dbName);
        return;
    }

    printf("Table '%s' dropped successfully from database '%s'.\n", tableName, dbName);
}

Kode ini adalah implementasi beberapa fungsi yang terkait dengan operasi pada database dan tabel dalam sebuah sistem manajemen basis data sederhana. Berikut adalah penjelasan singkat tentang setiap fungsi:

createDatabase(const char *dbName): Fungsi ini digunakan untuk membuat database baru. Fungsi ini akan membuat direktori untuk database baru dalam DATABASES_DIR (direktori basis data utama) menggunakan fungsi mkdir(). Jika direktori basis data utama belum ada, fungsi ini juga akan membuatnya. Fungsi ini akan memeriksa apakah batas maksimum database (MAX_DATABASES) telah tercapai. Jika ya, maka fungsi ini akan mencetak pesan bahwa batas maksimum telah tercapai. Database baru kemudian ditambahkan ke dalam array databases untuk melacak daftar database yang ada.

createTable(const char *dbName, const char *tableName, char *columns): Fungsi ini digunakan untuk membuat tabel baru dalam sebuah database. Fungsi ini memeriksa apakah database yang ditentukan (dbName) ada dalam array databases. Jika tidak, fungsi ini mencetak pesan bahwa database tidak ada. Fungsi ini juga memeriksa apakah tabel dengan nama yang sama (tableName) sudah ada dalam database. Jika tabel sudah ada, fungsi ini mencetak pesan bahwa tabel sudah ada. Fungsi ini mem-parse nama-nama kolom dari string columns yang dipisahkan oleh koma. Kolom-kolom ini kemudian disalin ke dalam tabel baru dan tabel baru ditambahkan ke dalam array tabel pada database yang sesuai. Selain itu, fungsi ini juga membuat file CSV untuk tabel baru dalam direktori database yang sesuai.

dropDatabase(const char *dbName): Fungsi ini digunakan untuk menghapus database. Fungsi ini memeriksa apakah direktori database yang ditentukan (dbName) ada. Jika tidak, fungsi ini mencetak pesan bahwa database tidak ada. Jika direktori ada, fungsi ini menggunakan fungsi rmdir() untuk menghapus direktori database.

dropTable(const char *dbName, const char *tableName): Fungsi ini digunakan untuk menghapus tabel dari sebuah database. Fungsi ini memeriksa apakah database yang ditentukan (dbName) ada dalam array databases. Jika tidak, fungsi ini mencetak pesan bahwa database tidak ada. Jika database ada, fungsi ini mencari indeks tabel dalam array tabel pada database yang sesuai. Jika tabel tidak ditemukan, fungsi ini mencetak pesan bahwa tabel tidak ada. Jika tabel ditemukan, fungsi ini menghapus tabel dari array tabel pada database dan juga menghapus file CSV untuk tabel tersebut.

Fungsi-fungsi tersebut merupakan bagian dari sistem manajemen basis data sederhana dan digunakan untuk mengelola operasi dasar pada database dan tabel seperti membuat, menghapus, dan memeriksa keberadaan.

D. Data Manipulation Language

void insertIntoTable(const char *dbName, const char *tableName, char *values)
{
    int dbIndex = findDatabaseIndex(dbName);
    if (dbIndex == -1)
    {
        printf("Database '%s' does not exist.\n", dbName);
        return;
    }

    Database *db = &databases[dbIndex];

    int tableIndex = findTableIndex(dbName, tableName);
    if (tableIndex == -1)
    {
        printf("Table '%s' does not exist in database '%s'.\n", tableName, dbName);
        return;
    }

    Table *table = &db->tables[tableIndex];

    // Parsing values
    char insertValues[MAX_COLUMNS][50];
    char *token = strtok(values, ",");
    int numValues = 0; // Initialize numValues to zero

    while (token != NULL && numValues < table->numColumns)
    {
        strcpy(insertValues[numValues], token);
        token = strtok(NULL, ",");
        numValues++;
    }

    if (numValues != table->numColumns)
    {
        printf("Incorrect number of values for table '%s'. Expected %d values.\n", tableName, table->numColumns);
        return;
    }

    // Append the values to the table's CSV file
    char tablePath[150];
    snprintf(tablePath, sizeof(tablePath), "%s%s/%s.csv", DATABASES_DIR, dbName, tableName);

    FILE *tableFile = fopen(tablePath, "a");
    if (tableFile == NULL)
    {
        printf("Failed to insert values into table '%s' in database '%s'.\n", tableName, dbName);
        return;
    }

    for (int i = 0; i < numValues; i++)
    {
        fprintf(tableFile, "%s", insertValues[i]);
        if (i != numValues - 1)
        {
            fprintf(tableFile, ",");
        }
    }

    fprintf(tableFile, "\n");
    fclose(tableFile);

    printf("Values inserted successfully into table '%s' in database '%s'.\n", tableName, dbName);
}
void selectFromTable(const char *dbName, const char *tableName)
{
    int dbIndex = findDatabaseIndex(dbName);
    if (dbIndex == -1)
    {
        printf("Database '%s' does not exist.\n", dbName);
        return;
    }

    Database *db = &databases[dbIndex];

    int tableIndex = findTableIndex(dbName, tableName);
    if (tableIndex == -1)
    {
        printf("Table '%s' does not exist in database '%s'.\n", tableName, dbName);
        return;
    }

    Table *table = &db->tables[tableIndex];

    // Read the table's CSV file and print the data
    char tablePath[150];
    snprintf(tablePath, sizeof(tablePath), "%s%s/%s.csv", DATABASES_DIR, dbName, tableName);

    FILE *tableFile = fopen(tablePath, "r");
    if (tableFile == NULL)
    {
        printf("Failed to open table '%s' in database '%s'.\n", tableName, dbName);
        return;
    }

    char line[1000];
    while (fgets(line, sizeof(line), tableFile))
    {
        printf("%s", line);
    }

    fclose(tableFile);
}

Kode tersebut mengimplementasikan dua fungsi terkait operasi pada tabel dalam sistem manajemen basis data sederhana, yaitu insertIntoTable dan selectFromTable. Berikut penjelasan singkat tentang kedua fungsi tersebut:

insertIntoTable(const char *dbName, const char *tableName, char *values): Fungsi ini digunakan untuk memasukkan nilai ke dalam tabel yang ada dalam database. Fungsi ini akan memeriksa apakah database yang ditentukan (dbName) ada dalam array databases. Jika tidak, fungsi ini akan mencetak pesan bahwa database tidak ada. Jika database ada, fungsi ini mencari indeks tabel dalam array tabel pada database yang sesuai. Jika tabel tidak ditemukan, fungsi ini mencetak pesan bahwa tabel tidak ada. Jika tabel ditemukan, fungsi ini akan mem-parsing nilai-nilai yang diberikan (values) yang dipisahkan oleh koma. Nilai-nilai ini kemudian disalin ke dalam array insertValues. Fungsi ini memeriksa apakah jumlah nilai yang dimasukkan sesuai dengan jumlah kolom dalam tabel. Jika tidak sesuai, fungsi ini mencetak pesan bahwa jumlah nilai yang dimasukkan tidak sesuai dengan jumlah kolom tabel. Jika jumlahnya sesuai, fungsi ini membuka file CSV tabel dan menambahkan nilai-nilai tersebut ke dalam file. Setelah selesai, file ditutup dan fungsi mencetak pesan bahwa nilai-nilai telah dimasukkan dengan sukses ke dalam tabel.

selectFromTable(const char *dbName, const char *tableName): Fungsi ini digunakan untuk membaca dan mencetak data dari tabel yang ada dalam database. Fungsi ini akan memeriksa apakah database yang ditentukan (dbName) ada dalam array databases. Jika tidak, fungsi ini akan mencetak pesan bahwa database tidak ada. Jika database ada, fungsi ini mencari indeks tabel dalam array tabel pada database yang sesuai. Jika tabel tidak ditemukan, fungsi ini mencetak pesan bahwa tabel tidak ada. Jika tabel ditemukan, fungsi ini membuka file CSV tabel dan membaca setiap baris dari file tersebut menggunakan fgets(). Setiap baris yang dibaca kemudian dicetak menggunakan printf(). Setelah selesai membaca file, file ditutup.

Kedua fungsi ini memanipulasi file CSV yang berisi data tabel untuk menyimpan dan membaca data.


E. Logging
skip

F. Reability
skip

G. Tambahan
Command redirection skip

H. Error Handling
Command error

I. Contenerization
Belum buat docker

J. Extra
Belum problem setter